from PIL import Image, ImageDraw, ImageFont
import random

picture_contents= [ line for line in open('./typesofpictures.txt')]
random_picture_content=random.choice(picture_contents)

image = Image.open('zucky.jpg')
font = ImageFont.truetype('RobotoSlab-Bold.ttf', size=25)
draw = ImageDraw.Draw(image) 
# starting position of the message
 
(x, y) = (50, 50)
message = "That Was A Great Picture Of Your "+random_picture_content
color = 'rgb(255, 255, 255)' # black color
draw.text((x, y), message, fill=color, font=font)

(x, y) = (450, 350)
end_phrase = 'I love it when you \n make money for me by \n posting pictures of your \n'+random_picture_content
color = 'rgb(255, 255, 255)' # white color
draw.text((x, y), end_phrase, fill=color, font=font)
 
draw = ImageDraw.Draw(image)
image.save('zuked.jpg')
